var express = require("express"),
    router = express.Router(),
    paypal = require('paypal-rest-sdk');
    
paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AYTCcSHb9DI8oFLbvGgP-cPzIa4HA18lKISpB6Bg-C5RzQxYFfpcP_VDBX0ND7adq9ZGtT86GEDTMA4u',
  'client_secret': 'EJj6EZ4Y72C9k4P4SmPoYevxktHPpw5DVKPb4lQoxlxkbXuqraCRYgEgPGYNhq6nIj-UfYj998rLgTdt'
});

router.get('/pay',function (req,res) {
    const create_payment_json = {
    "intent": "sale",
    "payer": {
        "payment_method": "paypal"
    },
    "redirect_urls": {
        "return_url": "https://printing-project-bharatnischal.c9users.io/success",
        "cancel_url": "https://printing-project-bharatnischal.c9users.io/cancel"
    },
    "transactions": [{
        "item_list": {
            "items": [{
                "name": 'code',
                "sku": "item",
                "price": "25.00",
                "currency": "USD",
                "quantity": 1
            }]
        },
        "amount": {
            "currency": "USD",
            "total": "25.00"
        },
        "description": "This is the payment description."
    }]
};

paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
        throw error;
    } else {
        payment.links.forEach(function (link) {
            if(link.rel === 'approval_url'){
                res.redirect(link.href);
            }
        });
    }
});
});

router.get('/success',function (req,res) {
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;
    const execute_payment_json = {
        "payer_id": payerId,
        "transactions": [{
            "amount": {
            "currency": "USD",
            "total": "25.00"
            }
        }]
    };
    
    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response);
            throw error;
        } else {
            console.log(JSON.stringify(payment));
            res.send('Success');
        }
    });
});

router.get("/cancel",function(req, res) {
    res.send("Cancelled");
});

module.exports = router;